import 'babel-core/register'
import 'babel-polyfill'

import express from 'express'
import { format } from 'date-fns'
import cors from 'cors'
import DB from './db-sql'

const app = express()
app.use(cors({ 
  "origin": "*",
  "methods": "OPTIONS,GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": true,
  "optionsSuccessStatus": 200
}))
app.use(express.json())

app.get('/', (req, res) => {
  return res
    .status(200)
    .send({ message: 'YAY! Congratulations! Your first endpoint is working' })
})

app.post('/registration', (req, res) => {
  const { name, login, password } = req.body

  if (name && login && password) {
    return DB.models.user
      .create({
        name,
        login,
        password,
        token: Date.now(),
      })
      .then(user => res.status(200).send({ result: user, error: null }))
  }

  return res
    .status(200)
    .send({ result: null, error: 'Name, login or password are missed!' })
})

app.post('/profileByToken', async (req, res) => {
  try {
    const { token } = req.body;
  
    if (token) {
      const user = await DB.models.user
        .findOne({
          where: {
            token,
          },
          include: [{
            model: DB.models.photo, as: 'photo'
          }]
        })

      if (!user) {
        return res.status(200).send({ result: null, error: 'User not found' })
      }

      let likesByPhoto;

      if (user && user.photo) {
        likesByPhoto = await DB.models.likesByPhoto
          .count({
            where: {
              owner: user.id
            }
          })
      } else {
        likesByPhoto = 0
      }

      const cardsCountSend = await DB.models.card
        .count({
          where: {
            owner: user.id
          }
        })

      const cardsCountReceived = await DB.models.card
        .count({
          where: {
            recipient: user.id
          }
        })

      const result = {
        name: user.name,
        token: user.token,
        registeredAt: format(Number(user.token), "dd.MM.yyyy"),
        points: user.points || 0,
        likesByPhoto: likesByPhoto,
        cardsCountSendByUser: cardsCountSend,
        cardsCountReceivedByUser: cardsCountReceived,
        user,
      }
      
      return res.status(200).send({
        result,
        error: null
      })
    }

    return res.status(200).send({ result: null, error: 'Token not exists' })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.post('/auth', cors(), async (req, res) => {
  const { login, password } = req.body

  if (login && password) {
    const userExists = await DB.models.user
      .findOne({
        where: {
          login,
        },
      })

    const user = await DB.models.user
      .findOne({
        where: {
          login,
          password
        },
      })

    if (!userExists) {
      return res.status(200).send({ result: null, error: 'Неверный логин'})
    }

    if (userExists && !user) {
      return res.status(200).send({ result: null, error: 'Неверный пароль'})
    }

    const result = {
      name: user.name,
      token: user.token,
    }

    return res.status(200).send({ result, error: null })
  }

  return res
    .status(200)
    .send({ result: null, error: 'Login or password are missed' })
})

// get all users
app.get('/users', async (req, res) => {
  const result = await DB.models.user.findAll()

  res.status(200).send({ result, error: null })
})

// change password
app.post('/users', async (req, res) => {
  try {
    const { token, password } = req.body;

    await DB.models.user
      .update({ password }, { where: { token: token } })

    const result = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })
  
    return res.status(200).send({ result, error: null })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.post('/pointsByUser', async (req, res) => {
  try {
    const { token, points } = req.body;

    const result = await DB.models.user
      .update({ points }, {
        where: {
          token: token,
        }
      })

    return res.status(200).send({ result, error: null })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.get('/photos', async (req, res) => {
  try {
    const photos = await DB.models.photo
      .findAll({
        include: [{ 
          model: DB.models.likesByPhoto, as: 'likes',
        }]
      })
    const photoWithLikes = photos.map(({ id, picture, owner, likes }) => ({
      id,
      picture,
      likes: likes ? likes.length : 0,
      owner
    }))
    
    return res.status(200).send({ result: photoWithLikes, error: null })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.post('/photos', async (req, res) => {
  try {
    const { picture, token } = req.body;

    const user = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })

    if (picture && token) {
      await DB.models.photo
        .create({
          picture,
          owner: user.id
        })
      
      const photos = await DB.models.photo
        .findAll({
          include: [{ 
            model: DB.models.likesByPhoto, as: 'likes',
          }]
        })

      const photoWithLikes = photos.map(({ id, picture, owner, likes }) => ({
        id,
        picture,
        likes: likes ? likes.length : 0,
        owner
      }))

      return res.status(200).send({ result: photoWithLikes, error: null })
    }

    return res.status(200).send({ result: null, error: 'Not picture or token' })
  } catch(error) {
    return res.status(200).send({ result: null, error })
  }
})

app.put('/photos', async (req, res) => {
  try {
    const { photoId, token } = req.body
    const user = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })

    const likeByUserAndPhoto = await DB.models.likesByPhoto
      .findOne({
        where: {
          photo: photoId,
          owner: user.id
        }
      })

    if (likeByUserAndPhoto && likeByUserAndPhoto.id) {
      await DB.models.likesByPhoto
        .destroy({
          where: {
            id: likeByUserAndPhoto.id
          }
        })
    } else {
      await DB.models.likesByPhoto
        .create({
          photo: photoId,
          owner: user.id
        })
  
    }
    const photos = await DB.models.photo
      .findAll({
        include: [{ 
          model: DB.models.likesByPhoto, as: 'likes',
        }]
      })

    const photosWithLikes = photos.map(({ id, picture, owner, likes }) => ({
        id,
        picture,
        likes: likes ? likes.length : 0,
        owner
      }))

    return res.status(200).send({ result: photosWithLikes, error: null })
  } catch(error) {
    return res.status(200).send({ result: null, error })
  }
})

app.post('/cardsByRecipient', async (req, res) => {
  try {
    const { token } = req.body;
    const user = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })

    const cards = await DB.models.card
      .findAll({
        where: {
          recipient: user.id,
        },
        include: [{ 
          model: DB.models.likesByCard, as: 'likes',
        }]
      })

    const cardWithLikes = cards.map(({ id, picture, title, owner, likes }) => ({
      id,
      picture,
      title,
      likes: likes ? likes.length : 0,
      owner
    }))

    return res.status(200).send({ result: cardWithLikes, error: null })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.get('/cards', async (req, res) => {
  try {
    const cards = await DB.models.card
      .findAll({
        include: [{ 
          model: DB.models.likesByCard, as: 'likes',
        }]
      })

    const cardWithLikes = cards.map(({ id, picture, title, owner, likes }) => ({
      id,
      picture,
      title,
      likes: likes ? likes.length : 0,
      owner
    }))

    return res.status(200).send({ result: cardWithLikes, error: null })
  } catch (error) {
    return res.status(200).send({ result: null, error })
  }
})

app.post('/cards', async (req, res) => {
  try {
    const { picture, token, email } = req.body;
    
    if (picture && token && email) {
      const user = await DB.models.user
        .findOne({ 
          where: {
            token: token
          }
        })

      const recipient = await DB.models.user
        .findOne({ 
          where: {
            login: email
          }
        })

      if (recipient) {
        await DB.models.card
          .create({
            picture,
            owner: user.id,
            recipient: recipient.id,
          })
        
        const cards = await DB.models.card
          .findAll({
            include: [{ 
              model: DB.models.likesByCard, as: 'likes',
            }]
          })
  
        const cardWithLikes = cards.map(({ id, picture, title, owner, likes }) => ({
          id,
          picture,
          title,
          likes: likes ? likes.length : 0,
          owner
        }))
  
        return res.status(200).send({ result: cardWithLikes, error: null })
      }
      return res.status(200).send({ result: null, error: 'Recipient not exists' })
    }

    return res.status(200).send({ result: null, error: 'Not picture, token or recipient' })
  } catch(error) {
    return res.status(200).send({ result: null, error })
  }
})

// toggle like
app.put('/cards', async (req, res) => {
  try {
    const { cardId, token } = req.body
    const user = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })

    const likeByUserAndCard = await DB.models.likesByCard
      .findOne({
        where: {
          card: cardId,
          owner: user.id
        }
      })

    if (likeByUserAndCard && likeByUserAndCard.id) {
      await DB.models.likesByCard
        .destroy({
          where: {
            id: likeByUserAndCard.id
          }
        })
    } else {
      await DB.models.likesByCard
        .create({
          card: cardId,
          owner: user.id
        })
  
    }
    const cards = await DB.models.card
      .findAll({
        include: [{ 
          model: DB.models.likesByCard, as: 'likes',
        }]
      })

    const cardWithLikes = cards.map(({ id, picture, title, owner, likes }) => ({
      id,
      picture,
      title,
      likes: likes ? likes.length : 0,
      owner
    }))

    return res.status(200).send({ result: cardWithLikes, error: null })
  } catch (error) {
    res.status(200).send({ result: null, error })
  }
})

app.get('/tracks', async (req, res) => {
  try {
    const tracks = await DB.models.track
      .findAll({
        include: [{ 
          model: DB.models.likesByTrack, as: 'likes',
        }]
      })

    const trackWithLikes = tracks.map(({ id, title, link, likes }) => ({
      id,
      title,
      link,
      likes: likes ? likes.length : 0
    }))
    
    res.status(200).send({ result: trackWithLikes, error: null })
  } catch (error) {
    res.status(200).send({ result: null, error })
  }
})

// toggle likes
app.put('/tracks', async (req, res) => {
  try {
    const { trackId, token } = req.body;
    const user = await DB.models.user
      .findOne({ 
        where: {
          token: token
        }
      })

    const likeByUserAndTrack = await DB.models.likesByTrack
      .findOne({
        where: {
          track: trackId,
          owner: user.id
        }
      })

    if (likeByUserAndTrack && likeByUserAndTrack.id) {
      await DB.models.likesByTrack
        .destroy({
          where: {
            id: likeByUserAndTrack.id
          }
        })
    } else {
      await DB.models.likesByTrack
        .create({
          track: trackId,
          owner: user.id
        })
    }

    const tracks = await DB.models.track
      .findAll({
        include: [{ 
          model: DB.models.likesByTrack, as: 'likes',
        }]
      })

    const trackWithLikes = tracks.map(({ id, title, link, likes }) => ({
      id,
      title,
      link,
      likes: likes ? likes.length : 0
    }))
    
    res.status(200).send({ result: trackWithLikes, error: null })
  } catch (error) {
    res.status(200).send({ result: null, error })
  }
})

app.listen(3000)

console.log('app running on port ', 3000)
