import Sequelize from 'sequelize'
import { HOST } from './config'

const Conn = new Sequelize(
  // database
  'a101',
  // login
  'root',
  // password
  '123',
  // options
  {
    dialect: 'mysql',
    host: HOST,
    // global params to define
    define: {
      charset: 'utf8',
      collate: 'utf8_general_ci',
      engine: 'MYISAM',
      timestamps: false,
    },
  }
)

const User = Conn.define(
  'user',
  {
    name: {
      type: Sequelize.STRING(50),
      field: 'name',
      allowNull: false,
    },
    login: {
      type: Sequelize.STRING(50),
      field: 'login',
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING(50),
      field: 'password',
      allowNull: false,
    },
    token: {
      type: Sequelize.STRING(50),
      field: 'token',
      allowNull: false,
    },
    points: {
      type: Sequelize.INTEGER,
      field: 'points',
      allowNull: true,
    },
  },
  {
    tableName: 'users',
  }
)

const Photo = Conn.define(
  'photo',
  {
    picture: {
      type: Sequelize.TEXT,
      field: 'picture',
      allowNull: false,
    },
    owner: {
      type: Sequelize.INTEGER,
      field: 'owner',
      allowNull: false,
    }
  },
  { tableName: 'photos' }
)

const LikesByPhoto = Conn.define(
  'likesByPhoto',
  {
    photo: {
      type: Sequelize.INTEGER,
      field: 'photo',
      allowNull: false,
    },
    owner: {
      type: Sequelize.INTEGER,
      field: 'owner',
      allowNull: false,
    },
  },
  { tableName: 'likesByPhoto' }
)

const Card = Conn.define(
  'card',
  {
    picture: {
      type: Sequelize.TEXT,
      field: 'picture',
      allowNull: false,
    },
    recipient: {
      type: Sequelize.INTEGER,
      field: 'recipient',
      allowNull: false,
    },
    owner: {
      type: Sequelize.INTEGER,
      field: 'owner',
      allowNull: false,
    },
  },
  { tableName: 'cards' }
)

const LikesByCard = Conn.define(
  'likesByCard',
  {
    card: {
      type: Sequelize.INTEGER,
      field: 'card',
      allowNull: false,
    },
    owner: {
      type: Sequelize.INTEGER,
      field: 'owner',
      allowNull: false,
    },
  },
  { tableName: 'likesByCard' }
)

const Track = Conn.define(
  'track',
  {
    title: {
      type: Sequelize.STRING(100),
      field: 'title',
      allowNull: false,
    },
    link: {
      type: Sequelize.STRING(150),
      field: 'link',
      allowNull: false,
    },
  },
  { tableName: 'tracks' }
)

const LikesByTrack = Conn.define(
  'likesByTrack',
  {
    track: {
      type: Sequelize.INTEGER,
      field: 'track',
      allowNull: false,
    },
    owner: {
      type: Sequelize.INTEGER,
      field: 'owner',
      allowNull: false,
    },
  },
  { tableName: 'likesByTrack' }
)

// Card.belongsTo(User, { foreignKey: 'owner' })

User.hasOne(Photo, {
  foreignKey: 'owner', as: 'photo'
})

Photo.hasMany(LikesByPhoto, {
  foreignKey: 'photo', as: 'likes'
})

Track.hasMany(LikesByTrack, {
  foreignKey: 'track', as: 'likes'
})

Card.hasMany(LikesByCard, {
  foreignKey: 'card', as: 'likes'
})

// LikesByTrack.belongsTo(Track, {
//   foreignKey: 'track',
// })

Conn.sync()

export default Conn
