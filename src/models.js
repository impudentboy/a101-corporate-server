import { Schema, SchemaTypes, Types, model } from 'mongoose'

const userSchema = Schema({
  _id: Types.ObjectId,
  name: String,
  email: String,
  password: String,
  token: String,
})

export const User = model('User', userSchema)

const cardSchema = Schema({
  _id: Types.ObjectId,
  picture: String,
  title: String,
  owner: SchemaTypes.ObjectId,
})

export const Card = model('Card', cardSchema)

const trackSchema = Schema({
  _id: Types.ObjectId,
  name: String,
  link: String,
})

export const Track = model('Track', trackSchema)
